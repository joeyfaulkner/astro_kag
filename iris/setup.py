import tensorflow as tf
import numpy as np

IRIS_TRAINING = "iris_training.csv"

IRIS_TEST="iris_test.csv"

training_set=tf.contrib.learn.datasets.base.load_csv(filename=IRIS_TRAINING,target_dtype=np.int)
test_set=tf.contrib.learn.datasets.base.load_csv(filename=IRIS_TEST,target_dtype=np.int)

x_train,x_test,y_train,y_test=training_set.data,test_set.data,training_set.target,test_set.target

print help(y_train)