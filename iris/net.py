import tensorflow as tf
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

n_nodes=10
df=pd.read_csv('iris.csv')
print df.columns
species=list(df['Species'].unique())
df['one_hot']=df['Species'].map(lambda x: np.eye(len(species))[species.index(x)])

inp=tf.placeholder(tf.float32,[None,4]) #the placeholder for the input covariates

weights_1=tf.Variable(tf.zeros([4,n_nodes]))

weights_2=tf.Variable(tf.zeros([n_nodes,3]))
shuffled = df.sample(frac=1)
trainingSet = shuffled[0:-100]
valid_set=shuffled[-100:-50]
testSet = shuffled[-50:]

bias_1=tf.Variable(tf.zeros([n_nodes]))
bias_2=tf.Variable(tf.zeros([3]))
y=tf.nn.softmax(tf.matmul(tf.sigmoid(tf.matmul(inp,weights_1)+bias_1),weights_2)+bias_2)


y_=tf.placeholder(tf.float32,[None,3])

cross_entropy=-tf.reduce_sum(y_*tf.log(y))

train_step=tf.train.AdamOptimizer(0.01).minimize(cross_entropy)

correct_prediction=tf.equal(tf.argmax(y,1),tf.argmax(y_,1))

accuracy=tf.reduce_mean(tf.cast(correct_prediction,"float"))


init=tf.initialize_all_variables()
sess=tf.Session()
sess.run(init)

print trainingSet.columns
keys=['Sepal.Length','Sepal.Width','Petal.Length','Petal.Width']
q=np.inf
#test=
z=[]
j=0
for i in range(1000):
	train=trainingSet.sample(50)

	sess.run(train_step,feed_dict={inp:[x for x in train[keys].values],
						y_:[x for x in train["one_hot"].as_matrix()]})
	
	q2=(sess.run(cross_entropy,feed_dict={inp:[x for x in valid_set[keys].values],
			y_:[ x for x in valid_set["one_hot"].as_matrix()]}))
	print q2
	if q<q2:
		j=j+1
		print 'sus'
		if j>50:
			break
	else:
		j=0
		q=q2
	z.append(sess.run(accuracy,feed_dict={inp:[x for x in testSet[keys].values],
		y_: [x for x in testSet["one_hot"].values]}))

print sess.run(accuracy,feed_dict={inp:[x for x in testSet[keys].values],
		y_: [x for x in testSet["one_hot"].values]})
plt.plot(z)
plt.show()