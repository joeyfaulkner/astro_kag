import tensorflow as tf
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df=pd.read_csv('iris.csv')
print df.columns
species=list(df['Species'].unique())
df['one_hot']=df['Species'].map(lambda x: np.eye(len(species))[species.index(x)])

inp=tf.placeholder(tf.float32,[None,4]) #the placeholder for the input covariates

weights=tf.Variable(tf.zeros([4,3]))

shuffled = df.sample(frac=1)
trainingSet = shuffled[0:len(shuffled)-50]
testSet = shuffled[len(shuffled)-50:]

bias=tf.Variable(tf.zeros([3]))

y=tf.nn.softmax(tf.matmul(inp,weights)+bias)

y_=tf.placeholder(tf.float32,[None,3])

cross_entropy=-tf.reduce_sum(y_*tf.log(y))

train_step=tf.train.AdamOptimizer(0.01).minimize(cross_entropy)

correct_prediction=tf.equal(tf.argmax(y,1),tf.argmax(y_,1))

accuracy=tf.reduce_mean(tf.cast(correct_prediction,"float"))


init=tf.initialize_all_variables()
sess=tf.Session()
sess.run(init)

print trainingSet.columns
keys=['Sepal.Length','Sepal.Width','Petal.Length','Petal.Width']
q=[]
for i in range(1000):
	train=trainingSet.sample(50)

	sess.run(train_step,feed_dict={inp:[x for x in train[keys].values],
						y_:[x for x in train["one_hot"].as_matrix()]})

	q.append(sess.run(accuracy,feed_dict={inp:[x for x in testSet[keys].values],
		y_: [x for x in testSet["one_hot"].values]}))

plt.plot(q)
plt.show()